const express = require('express');
const router = express.Router();
const HelloController = require('../controllers/helloController');

router.get('/',HelloController.get);
router.post('/',HelloController.post);
router.put('/',HelloController.put);
router.delete('/',HelloController.delete);
//kalo pake ('/') di index
// router.get('/ebitneygo',HelloController.get);
// router.post('/ebitneygo',HelloController.post);
// router.put('/ebitneygo',HelloController.put);
// router.delete('/ebitneygo',HelloController.delete);

module.exports = router;