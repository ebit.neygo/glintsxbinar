const Geometry = require("./geometry");

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, "3D");

    if (this.constructor == ThreeDimention) {
      throw new Error("Can not declare object!");
    }
  }

  // Overridding
  introduce() {
    super.introduce();
    console.log(`This is ${this.type}!`);
  }

  calculateArea() {
    console.log(`${this.name} Area!`);
  }

  calculateCircumference() {
    console.log(`${this.name} Circumference!`);
  }
  calculateVolume() {
    console.log(`${this.name} Volume!`);
  }
}

module.exports = ThreeDimention;
