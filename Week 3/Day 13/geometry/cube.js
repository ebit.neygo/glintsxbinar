const ThreeDimention = require("./threeDimention");

class Cube extends ThreeDimention {
  constructor(sisi) {
    super("Cube");

    this.side = sisi;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateArea() {
    super.calculateArea();
    let area = Math.pow(this.side,2);

    console.log(`${this.name} area is ${area} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circumference = 12*(this.side);
    console.log(`${this.name} circumference is ${circumference} cm2 \n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume = Math.pow(this.side,3);
    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }
}
module.exports = Cube;
