const ThreeDimention = require("./threeDimention");

class Tube extends ThreeDimention {
  constructor(radius,tinggi) {
    super("Tube");

    this.radius = radius;
    this.height = tinggi;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateArea() {
    super.calculateArea();
    let area = Math.PI* Math.pow(this.radius,2);

    console.log(`${this.name} base area is ${area} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circleCir = 2 * Math.PI*this.radius;
    let rectCir = 2 * (circleCir + this.height)
    let circumference = 2 * circleCir + rectCir;
    console.log(`${this.name} circumference is ${circumference} cm2 \n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume =Math.PI * Math.pow(this.radius,2) * this.height;
    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }
}
module.exports = Tube;
