const ThreeDimention = require("./threeDimention");

class Cone extends ThreeDimention {
  constructor(radius,tinggi) {
    super("Cone");

    this.radius = radius;
    this.height = tinggi;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateArea() {
    super.calculateArea();
    let area = Math.PI * Math.pow(this.radius,2);

    console.log(`${this.name} base area is ${area} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circumference = 2 *Math.PI*(this.radius);
    console.log(`${this.name} base circumference is ${circumference} cm2 \n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume = 1/3 * Math.pow(this.radius,2) * this.height;
    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }
}
module.exports = Cone;
