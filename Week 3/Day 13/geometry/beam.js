const ThreeDimention = require("./threeDimention");

class Beam extends ThreeDimention {
  constructor(panjang,lebar,tinggi) {
    super("Beam");

    this.length = panjang;
    this.height = tinggi;
    this.width = lebar;
  }

  // Overloading
  introduce(who) {
    super.introduce();
    console.log(`${who}, This is ${this.name}!`);
  }

  // Overridding
  calculateArea() {
    super.calculateArea();
    let area = this.length * this.width;

    console.log(`${this.name} area is ${area} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circumference = 2*(this.length+this.width);
    console.log(`${this.name} circumference is ${circumference} cm2 \n`);
  }
  calculateVolume() {
    super.calculateVolume();
    let volume = this.length*this.width*this.height;
    console.log(`${this.name} volume is ${volume} cm3 \n`);
  }
}
module.exports = Beam;
