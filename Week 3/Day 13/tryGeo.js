// const { Square, Rectangle, Beam, Cone } = require("./geometry");

const Beam = require('./geometry/beam');
const Cube = require('./geometry/cube');
const Tube = require('./geometry/tube');
const Cone = require('./geometry/cone');
const Rectangle = require('./geometry/rectangle');
const Square = require('./geometry/square');
const Triangle = require('./geometry/triangle');;

let beam1 = new Beam(10,11,12);
let cube2 = new Cube(10);
let tube3 = new Tube(10,21);
let cone4 = new Cone(10,21);
let rectangle5 = new Rectangle(11,8);
let square6 = new Square(10);
let triangle7 = new Triangle(7,11);


// call all
beam1.calculateArea();
beam1.calculateCircumference();
beam1.calculateVolume();

cube2.calculateArea();
cube2.calculateCircumference();
cube2.calculateVolume();

tube3.calculateArea();
tube3.calculateCircumference();
tube3.calculateVolume();

cone4.calculateArea();
cone4.calculateCircumference();
cone4.calculateVolume();

rectangle5.calculateArea();
rectangle5.calculateCircumference();
// rectangle5.calculateVolume();

square6.calculateArea();
square6.calculateCircumference();
// square6.calculateVolume();

triangle7.calculateArea();
triangle7.calculateCircumference();
// triangle7.calculateVolume();