const fs = require("fs");

//make promise object

const readFile = (tulisan, encode) =>
  new Promise((success, failed) => {
    fs.readFile(tulisan, encode, (err, content) => {
      if (err) {
        failed(err);
      } else {
        return success(content);
      }
    });
  });
let x = 1;
  // readFile("./contents/content1.txt", "utf-8")
  // .then((content1) => {
  //   console.log(content1);
  //   return readFile("./contents/content2.txt", "utf-8");
  // })
  // .then((content2) => {
  //   console.log(content2);
  //   return readFile(`./contents/content13.txt`, "utf-8");
  // })
  // .then((content3)=>{
  //     console.log(content3);
  // })
  // .catch((err) => console.log(err));
let str = '';
  function readR(x,content){
    readFile(`./contents/content${x}.txt`, "utf-8")
    .then((content) => {
      str = str + content;
      if(x>=10){
        console.log(str);
        return 0;
      }
      x++;
      readR(x,content);
      // return readFile("./contents/content2.txt", "utf-8");
    })
    .catch((err)=>console.log(str,err));
  }
readR(x);
