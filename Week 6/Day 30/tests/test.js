const request = require("supertest");
const app = require("../index");
const { user, barang, transaksi } = require("../models");

beforeAll(async () => {
  await Promise.all([user.deleteMany(), transaksi.deleteMany()]);
});

describe("Auth Test", () => {
  describe("/auth/signup POST", () => {
    it("It Should create a user and return the broken", async () => {
      const res = await request(app).post("/auth/signup").send({
        email: "ebzztuiuiygo@gssail.xyz",
        password: "Qweee123.",
        confirmPassword: "Qweee123.",
        name: "Riwaduiuiui",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("token");
    });
    it("It should error when create a user", async () => {
      const res = await request(app).post("/auth/signup").send({
        email: "ebzztuiuiygo@gssail.xyz",
        password: "Qweee123.",
        confirmPassword: "Qweee123.",
        name: "Riwaduiuiui",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("User Can't be Created");
    });
  });
  
});
