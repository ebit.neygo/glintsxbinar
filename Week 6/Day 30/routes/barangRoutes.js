const express = require("express"); // Import express
const router = express.Router(); // Make a router
const passport = require("passport");

// Import middlewares
const barangValidator = require("../middlewares/validators/barangValidator");

// Import controller
const barangController = require("../controllers/barangController");
require("../middlewares/auth");

// If POST (/barang)
// Then, go to transaksiValidator.create
// If in the transaksiValidator.create can run the next(), it will go to transaksiController.create
router.post("/", barangValidator.createbarang, barangController.create);
router.get(
  "/",
  async (req, res, next) => {
    passport.authorize("user", (err, user, info) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err.message,
        });
      }
      if (!user) {
        return res.status(403).json({
          message: info.message,
        });
      }
      req.user = user;
      next();
    })(req, res, next);
  },
  barangController.getAll
);
router.get("/:id", barangController.getOne);
router.put("/:id", barangValidator.updateBarang, barangController.updateBarang);
router.delete("/:id", barangController.deleteBarang);

module.exports = router; // Export router
