const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const pemasokValidator = require("../middlewares/validators/pemasokValidator");

// Import controller
const pemasokController = require("../controllers/pemasokController");

// If POST (/barang)
// Then, go to transaksiValidator.create
// If in the transaksiValidator.create can run the next(), it will go to transaksiController.create
router.post("/", pemasokController.create);
router.get("/", pemasokController.getAll);
router.get("/:id",pemasokValidator.getOne, pemasokController.getOne);
router.delete('/:id',pemasokController.deletePemasok);
router.put('/:id',pemasokValidator.updatePemasok, pemasokController.updatePemasok);

module.exports = router; // Export router
