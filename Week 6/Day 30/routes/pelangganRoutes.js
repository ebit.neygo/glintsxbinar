const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const pelangganValidator = require("../middlewares/validators/pelangganValidator");

// Import controller
const pelangganController = require("../controllers/pelangganController");

// If POST (/barang)
// Then, go to transaksiValidator.create
// If in the transaksiValidator.create can run the next(), it will go to transaksiController.create
router.post("/", pelangganController.create);
router.get("/", pelangganController.getAll);
router.get("/:id",pelangganValidator.getOne, pelangganController.getOne);
router.delete('/:id',pelangganController.deletePelanggan);
router.put('/:id',pelangganValidator.updatePelanggan, pelangganController.updatePelanggan);

module.exports = router; // Export router
