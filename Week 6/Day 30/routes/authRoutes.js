const express = require("express");
const passport = require("passport");

//import validator

//import controller
const authController = require("../controllers/authController");
const authValidator = require("../middlewares/validators/authValidator");
//import middleware
const auth = require("../middlewares/auth");

//make router for auth
const router = express.Router();
router.post(
  "/signup",
  authValidator.signup,
  auth.signup,
  authController.getToken
);
router.post(
  "/signin",
  authValidator.signin,
  auth.signin,
  authController.getToken
);
module.exports = router;
