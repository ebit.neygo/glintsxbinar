const jwt = require("jsonwebtoken");

class AuthController {
  //if user login/signup
  async getToken(req, res) {
    try {
      ///make body variable
      const body = {
        user:{
          id:req.user._id,
        },
      };
      const token = jwt.sign(body,process.env.JWT_TOKEN,{
        expiresIn:'60d',
      });

      //if success
      return res.status(200).json({
        message: "Success",
        token,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}
module.exports = new AuthController();
