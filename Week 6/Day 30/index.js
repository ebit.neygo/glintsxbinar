require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
// Express
const express = require("express");
const fileUpload = require("express-fileupload");

// Import routes
const barangRoutes = require("./routes/barangRoutes");
const transaksiRoutes = require("./routes/transaksiRoutes");
const pemasokRoutes = require("./routes/pemasokRoutes");
const pelangganRoutes = require("./routes/pelangganRoutes");
const authRoutes = require("./routes/authRoutes");

// Make express app
const app = express();

// Body-parser to read req.body
app.use(express.json()); // Enable req.body JSON type
app.use(
  express.urlencoded({
    extended: true,
  })
); // Support urlencode body

// To read form-data request
app.use(fileUpload());

// Set static file directory
app.use(express.static("public"));

// Make routes
app.use("/barang", barangRoutes);
app.use("/transaksi", transaksiRoutes);
app.use("/pemasok", pemasokRoutes);
app.use("/pelanggan", pelangganRoutes);
app.use("/auth", authRoutes);

// Running server
if (process.env.NODE_ENV !== "test") {
  
  app.listen(5000, () => console.log("Server running on 5000"));
}

module.exports = app;
