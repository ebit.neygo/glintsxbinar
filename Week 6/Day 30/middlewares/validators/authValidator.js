const validator = require("validator");

exports.signup = async (req, res, next) => {
  try {
    let errors = [];

    //check email or not
    if (!validator.isEmail(req.body.email)) {
      errors.push("Email Field must be email");
    }
    //check password strength
    if (!validator.isStrongPassword(req.body.password)) {
      errors.push("Password needs (upper & lower case), mumber and symbol");
    }
    //password confirmation
    if (req.body.confirmPassword != req.body.password) {
      errors.push("Password confirmation must be same as password");
    }
    //if errors
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};
exports.signin = async (req, res, next) => {
    try {
      let errors = [];
  
      //check email or not
      if (!validator.isEmail(req.body.email)) {
        errors.push("Email Field must be email");
      }
      //check password strength
      if (!validator.isStrongPassword(req.body.password)) {
        errors.push("Password needs (upper & lower case), mumber and symbol");
      }
      //password confirmation
    //   if (req.body.confirmPassword != req.body.password) {
    //     errors.push("Password confirmation must be same as password");
    //   }
      //if errors
      if (errors.length > 0) {
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
