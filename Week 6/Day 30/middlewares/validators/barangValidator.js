const mongoose = require("mongoose");
const validator = require("validator");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.createbarang = async (req, res, next) => {
  // Initialita
  let errors = [];

  // Check id_pemasok
  if (!mongoose.Types.ObjectId.isValid(req.body.id_pemasok)) {
    errors.push(
      "id_pemasok is not valid and must be 24 character & hexadecimal"
    );
  }

  // If params error
  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  // find pemasok
  let dataPemasok = await pemasok.findOne({ _id: req.body.id_pemasok });

  // If data pemasok not found
  if (!dataPemasok) {
    errors.push("Pemasok not found");
  }

  // Check harga is number
  if (!validator.isNumeric(req.body.harga)) {
    errors.push("Harga must be a number");
  }

  // If errors length > 0, it will make errors message
  if (errors.length > 0) {
    // Because bad request
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  req.body.pemasok = req.body.id_pemasok;

  // It means that will be go to the next middleware
  next();
};
module.exports.updateBarang = async (req, res,next)=>{
  try {
      //find pemasok barang
      let findPemasok = await pemasok.findOne({ _id: req.body.id_pemasok });
      let errors = [];

      //if pemasok barang not found
      if(!findPemasok){
          errors.push('Pemasok Not Found');
      }
      //check harga (must be a number)
      if(!validator.isNumeric(req.body.harga)){
          errors.push('Harga must be a number');
      }
      //handle all table error
      if(errors.length>0){
          return res.status(400).json({
              message: errors.join(', '),
          });
      }

      req.body.pemasok = req.body.id_pemasok;
      //go to next middleware function
      next();
  } catch (e) {
      return res.status(500).json({
          message:'Internal Server Error (Validator)',
          error: e,
      });
  }
}
