const mongoose = require("mongoose");
const validator = require("validator");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.create = async (req, res, next) => {
  // Initialita


  // It means that will be go to the next middleware
  next();
};
exports.getOne = (req, res, next) => {
  // Check parameter is valid or not
  console.log(req.params.id);
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).json({
      message: "Parameter is not valid and must be 24 character & hexadecimal",
    });
  }

  next();
};
module.exports.updatePemasok = async (req, res,next)=>{
  try {
      //find pemasok barang
      let findPemasok = await pemasok.findOne({ _id: req.params.id });
      let errors = [];
      //if pemasok barang not found
      if(!findPemasok){
          errors.push('Pemasok Not Found');
      }
      //check harga (must be a number)
      //handle all table error
      if(errors.length>0){
          return res.status(400).json({
              message: errors.join(', '),
          });
      }
      req.body.photo = req.body.image;
      //go to next middleware function
      next();
  } catch (e) {
      return res.status(500).json({
          message:'Internal Server Error (Validator)',
          error: e,
      });
  }
}
