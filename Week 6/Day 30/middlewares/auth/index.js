const passport = require("passport"); //import password
const LocalStrategy = require("passport-local").Strategy; //import localstrategy
const bcrypt = require("bcrypt"); //import bcrypt
const JWTstrategy = require("passport-jwt").Strategy; //import jwt strategy
const ExtractJWT = require("passport-jwt").ExtractJwt; //import extract jwt
const { user } = require("../../models"); //import user model

//===
exports.signup = (req, res, next) => {
  passport.authenticate("signup", { session: false }, (err, user, info) => {
    //if error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }

    //if user false
    if (!user) {
      return res.status(401).json({
        message: info.message,
      });
    }

    //create req.user by user value
    req.user = user;

    next();
  })(req, res, next);
};
passport.use(
  "signup",
  new LocalStrategy(
    {
      usernameField: "email", //username is come from req.body.email
      passwordField: "password", //password is come from req.body.password
      passReqToCallback: true, //enable to read req.body/req.params/req.
    },
    async (req, email, password, done) => {
      try {
        // console.log(email);
        let userSignUp = await user.create(req.body);
        //if success
        return done(null, userSignUp, { message: "User Can Be Created" });
      } catch (e) {
        return done(null,false, {
          message: "User Can't be Created",
        });
      }
    }
  )
);

exports.signin = (req, res, next) => {
  passport.authenticate("signin", { session: false }, (err, user, info) => {
    //if error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }
    //if user false
    if (!user) {
      return res.status(401).json({
        message: info.message,
      });
    }

    //create req.user by user value
    req.user = user;

    next();
  })(req, res, next);
};
passport.use(
  "signin",
  new LocalStrategy(
    {
      usernameField: "email", //username is come from req.body.email
      passwordField: "password", //password is come from req.body.password
      passReqToCallback: true, //enable to read req.body/req.params/req.
    },
    async (req, email, password, done) => {
      try {
        let userSignIn = await user.findOne({ email });
        //if usersignin not exist
        if (!userSignIn) {
          return done(null, false, {
            message: "Email Not Found",
          });
        }
        let validate = await bcrypt.compare(password, userSignIn.password);
        if (!validate) {
          return done(null, false, {
            message: `Wrong Password`,
          });
        }
        //if success
        return done(null, userSignIn, { message: "User Can Sign In" });
      } catch (e) {
        return done(null, false, {
          message: `User Can't Sign In`,
          error: e,
        });
      }
    }
  )
);

//===
exports.admin = (req, res, next) => {
  passport.authorize("admin", (err, user, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }
    if (!user) {
      return res.status(403).json({
        message: info.message,
      });
    }
    req.user = user;
    next();
  })(req, res, next);
};
passport.use(
  "admin",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_TOKEN,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        let userSignIn = await user.findOne({ _id: token.user.id });

        //if  admin login
        if (userSignIn.role.includes("admin")) {
          return done(null, token.user);
        }
        //if user login
        return done(null, false, { message: "Not Authorized" });
      } catch (e) {
        return done(null, false, {
          message: `Not Authorized`,
          // error: e,
        });
      }
    }
  )
);
//user auth
exports.user = (req, res, next) => {
  passport.authorize("user", (err, user, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }
    if (!user) {
      return res.status(403).json({
        message: info.message,
      });
    }
    req.user = user;
    next();
  })(req, res, next);
};
passport.use(
  "user",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_TOKEN,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        let userSignIn = await user.findOne({ _id: token.user.id });

        //if  admin login
        if (userSignIn.role.includes("user")) {
          return done(null, token.user);
        }
        //if user login
        return done(null, false, { message: "Not Authorized" });
      } catch (e) {
        return done(null, false, {
          message: "Not Authorized",
          // error: e,
        });
      }
    }
  )
);

//admin or not
exports.adminOrUser = (req, res, next) => {
  passport.authorize("adminOrUser", (err, user, info) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }
    if (!user) {
      return res.status(403).json({
        message: info.message,
      });
    }
    req.user = user;
    next();
  })(req, res, next);
};
passport.use(
  "adminOrUser",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_TOKEN,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        let userSignIn = await user.findOne({ _id: token.user.id });

        //if  admin login
        if (
          userSignIn.role.includes("user") ||
          userSignIn.role.includes("admin")
        ) {
          return done(null, token.user);
        }
        //if user login
        return done(null, false, { message: "Not Authorized" });
      } catch (e) {
        return done(null, false, {
          message: "Not Authorized",
          // error: e,
        });
      }
    }
  )
);
