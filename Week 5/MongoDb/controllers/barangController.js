const { ObjectId } = require("mongodb"); // Import ObjectId
const connection = require("../models");
// const dbConnection = connection.db("penjualan_afternoon"); // Connect to database penjualan_morning
// const barang = dbConnection.collection("barang"); // Connect to table/collection transaksi

class BarangController {
    // Get All Data
    async getAll(req, res) {
        const dbConnection = connection.db("penjualan_afternoon"); // Connect to database penjualan_morning
        const barang = dbConnection.collection("barang"); // Connect to table/collection transaksi
        try {
            let data = await barang.find({}).toArray(); // Get all data from transaksi table

            // If no data
            if (data.length === 0) {
                return res.status(404).json({
                    message: "Barang Not Found",
                });
            }

            // If success
            return res.status(200).json({
                message: "Success",
                data,
            });
        } catch (e) {
            // If failed
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }

    // Get One Data
    async getOne(req, res) {
        const dbConnection = connection.db("penjualan_afternoon"); // Connect to database penjualan_morning
        const barang = dbConnection.collection("barang"); // Connect to table/collection transaksi
        try {
            // Find one data
            let data = await barang.findOne({
                _id: new ObjectId(req.params.id),
            });

            // If success
            return res.status(200).json({
                message: "Success",
                data,
            });
        } catch (e) {
            // If failed
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }

    // Create data
    async create(req, res) {
        const dbConnection = connection.db("penjualan_afternoon"); // Connect to database penjualan_morning
        const barang = dbConnection.collection("barang"); // Connect to table/collection transaksi
        try {
            // Insert data transaksi
            let data = await barang.insertOne({
                pemasok: req.body.pemasok,
                nama: req.body.nama,
                harga: req.body.harga,
            });

            // If success
            return res.status(200).json({
                message: "Success",
                data: data.ops[0],
            });
        } catch (e) {
            console.log(e);
            // If failed
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }

    // Update data
    async update(req, res) {
        const dbConnection = connection.db("penjualan_afternoon"); // Connect to database penjualan_morning
        const barang = dbConnection.collection("barang"); // Connect to table/collection transaksi
        try {
            // Update data transaksi
            await barang.updateOne(
                {
                    _id: new ObjectId(req.params.id),
                },
                {
                    $set: {
                        pemasok: req.body.pemasok,
                        nama: req.body.nama,
                        harga: req.body.harga,
                    },
                }
            );

            // Find data that updated
            let data = await barang.findOne({
                _id: new ObjectId(req.params.id),
            });

            // If success
            return res.status(200).json({
                message: "Success",
                data,
            });
        } catch (e) {
            // If failed
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }

    // Delete Data
    async delete(req, res) {
        const dbConnection = connection.db("penjualan_afternoon"); // Connect to database penjualan_morning
        const barang = dbConnection.collection("barang"); // Connect to table/collection transaksi
        try {
            // delete data depends on req.params.id
            let data = await barang.deleteOne({
                _id: new ObjectId(req.params.id),
            });

            // If success
            return res.status(200).json({
                message: "Success to delete barang",
            });
        } catch (e) {
            // If failed
            return res.status(500).json({
                message: "Internal Server Error",
                error: e,
            });
        }
    }
}

module.exports = new BarangController();