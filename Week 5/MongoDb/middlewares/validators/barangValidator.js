const validator = require("validator");
const { ObjectId } = require("mongodb");
const connection = require("../../models");

module.exports.create = async (req, res, next) => {
    const penjualan = connection.db("penjualan_afternoon"); // Connect to penjualan database
    
    try {
        // Get barang and pelanggan
        let findData = await Promise.all([
            penjualan.collection("pemasok").findOne({
                _id: ObjectId(req.body.id_pemasok),
            }),
        ]);

        // Create errors variable
        let errors = [];

        // If barang not found
        if (!findData[0]) {
            errors.push("Pemasok Not Found");
        }

        if (!validator.isNumeric(req.body.harga)) {
            errors.push("Harga must be a number");
        }

        // If errors length > 0, it will make errors message
        if (errors.length > 0) {
            // Because bad request
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // add some req.body for used in Controller
        req.body.pemasok = findData[0];

        // It means that will be go to the next middleware
        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e,
        });
    }
};

module.exports.update = async (req, res, next) => {
    const penjualan = connection.db("penjualan_afternoon"); // Connect to penjualan database
    const barang = penjualan.collection("barang"); // Connect to barang collection / table
    
    try {    
        // Get barang and pelanggan
        let findData = await Promise.all([
            penjualan.collection("pemasok").findOne({
                _id: new ObjectId(req.body.id_pemasok),
            }),
            barang.findOne({
                _id: new ObjectId(req.params.id),
            }),
        ]);

        
        // Create errors variable
        let errors = [];

        // If barang not found
        if (!findData[0]) {
            errors.push("Pemasok Not Found");
        }

        // If pelanggan not found
        if (!findData[1]) {
            errors.push("Barang Not Found");
        }

        if (!validator.isNumeric(req.body.harga)) {
            errors.push("Harga must be a number");
        }

        // If errors length > 0, it will make errors message
        if (errors.length > 0) {
            // Because bad request
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        // add some req.body for used in Controller
        req.body.pemasok = findData[0];
        // It means that will be go to the next middleware
        next();
    } catch (e) {
        console.log(e)
        return res.status(500).json({
            message: "Internal Server Error",
            error: e,
        });
    }
};