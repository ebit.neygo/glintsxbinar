const validator = require("validator");
const { ObjectId } = require("mongodb");
const connection = require("../../models");

module.exports.create = async (req, res, next) => {
    const penjualan = connection.db("penjualan_afternoon"); // Connect to penjualan database
    
    try {
        // Create errors variable
        let errors = [];

        // If errors length > 0, it will make errors message
        if (errors.length > 0) {
            // Because bad request
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        next();
    } catch (e) {
        return res.status(500).json({
            message: "Internal Server Error",
            error: e,
        });
    }
};

module.exports.update = async (req, res, next) => {
    const penjualan = connection.db("penjualan_afternoon"); // Connect to penjualan database
    const pemasok = penjualan.collection("pemasok"); // Connect to barang collection / table
    
    try {    
        // Get barang and pelanggan
        let findData = await Promise.all([
            pemasok.findOne({
                _id: new ObjectId(req.params.id),
            }),
        ]);

        
        // Create errors variable
        let errors = [];

        // If barang not found
        if (!findData[0]) {
            errors.push("Pemasok Not Found");
        }

        // If errors length > 0, it will make errors message
        if (errors.length > 0) {
            // Because bad request
            return res.status(400).json({
                message: errors.join(", "),
            });
        }

        next();
    } catch (e) {
        console.log(e)
        return res.status(500).json({
            message: "Internal Server Error",
            error: e,
        });
    }
};