require('dotenv').config({
    path: `.env.${process.env.NODE_ENV}`
  }) // Config environment
  const express = require("express"); // Import express

  // Make express app
  const app = express();
  
  // Import router
  const transaksiRoutes = require("./routes/transaksiRoutes");
  const barangRoutes = require("./routes/barangRoutes");
  const pemasokRoutes = require("./routes/pemasokRoutes");
  const pelangganRoutes = require("./routes/pelangganRoutes");
  
  // Enable body parser
  app.use(express.json());
  app.use(
    express.urlencoded({
      extended: false,
    })
  );
  
  // Make routes
  app.use("/transaksi", transaksiRoutes);
  app.use("/barang", barangRoutes);
  app.use("/pemasok", pemasokRoutes);
  app.use("/pelanggan", pelangganRoutes);
  
  // Run server
  app.listen(3000, () => console.log("Server running on 3000!"));