const express = require("express"); // Import express
const validator = require("validator");

// Import validator
const pemasokValidator = require("../middlewares/validators/pemasokValidator");

// Import controller
const pemasokController = require("../controllers/pemasokController");

// Make router
const router = express.Router();

// Get All Data
router.get("/", pemasokController.getAll);

// Create data
router.post("/", pemasokValidator.create, pemasokController.create);

// Get One Data
router.get("/:id", pemasokController.getOne);

// Update Data
router.put("/:id", pemasokValidator.update, pemasokController.update);

// Delete One Data
router.delete("/:id", pemasokController.delete);

// Export router
module.exports = router;