const express = require("express"); // Import express
const validator = require("validator");

// Import validator
const barangValidator = require("../middlewares/validators/barangValidator");

// Import controller
const barangController = require("../controllers/barangController");

// Make router
const router = express.Router();

// Get All Data
router.get("/", barangController.getAll);

// Create data
router.post("/", barangValidator.create, barangController.create);

// Get One Data
router.get("/:id", barangController.getOne);

// Update Data
router.put("/:id", barangValidator.update, barangController.update);

// Delete One Data
router.delete("/:id", barangController.delete);

// Export router
module.exports = router;