function evenNumbers(array, number) {
  // good luck
  let arr = [];
  for (i = 0; i < array.length; i++) {
      if (array[array.length-i-1] % 2 == 0 && number>0) {
        arr.unshift(array[array.length-i-1]);
        number--;
      }
  }

  return arr;
}

console.log(evenNumbers([1, 2, 3, 4, 5, 6, 7, 8, 9], 3));
// evenNumbers([1, 2, 3, 4, 5, 6, 7, 8, 9], 3), [4, 6, 8];
console.log(evenNumbers([-22, 5, 3, 11, 26, -6, -7, -8, -9, -8, 26], 2));
// evenNumbers([-22, 5, 3, 11, 26, -6, -7, -8, -9, -8, 26], 2), [-8, 26];
console.log(evenNumbers([6, -25, 3, 7, 5, 5, 7, -3, 23], 1));
// evenNumbers([6, -25, 3, 7, 5, 5, 7, -3, 23], 1), [6];

//23.25
//00.04
