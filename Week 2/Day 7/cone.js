// // Import readline
const { rl } = require("./utils"); // Import index to run rl on this file

/*function to calculate cone */
function calculateCone(radius, height) {
    let volume = Math.PI * Math.pow(radius, 2) * height / 3;
    return volume;
}

function inputRadius() {
    rl.question(`Radius (cm): `, rad => {
        if (!isNaN(rad) && (rad > 0)) {
            inputHeight(rad);
        } else {
            console.log(`Radius should be in number!`);
            inputRadius();
        }
    })
}

function inputHeight(rad) {
    rl.question(`Height (cm): `, hgt => {
        if (!isNaN(hgt) && (hgt > 0)) {
            console.log(`Volume cone is (cm): ${calculateCone(rad,hgt)}`);
            rl.close()
        } else {
            console.log(`Radius should be in number!`);
            inputHeight(rad);
        }
    })
}

module.exports = { calculateCone, inputRadius, inputHeight }