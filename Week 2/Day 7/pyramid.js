const { rl } = require("./utils");

function calculateTriangularPrism(length, width, height) {
    return ((length * width * height) / 3);
}

const invalidInputText = "should be in number!";

const startCalculatePyramid = () => inputLength();

const inputLength = () => {
    rl.question("Base length (cm): ", length => {
        if (!isNaN(length) && (length > 0)) {
            inputWidth(length);
        } else {
            console.log("Base width " + invalidInputText);
            inputLength();
        }
    })
}

const inputWidth = (length) => {
    rl.question("Base width (cm): ", width => {
        if (!isNaN(width) && (width > 0)) {
            inputHeight(length, width);
        } else {
            console.log("Base width " + invalidInputText);
            inputWidth(length);
        }

    })
}

const inputHeight = (length, width) => {
    rl.question("Pyramid height (cm): ", height => {
        if (!isNaN(height) && (height > 0)) {
            console.log(`Volume of the pyramid is (cm): ${calculateTriangularPrism(length,width,height)}`);
            rl.close();
        } else {
            console.log("Pyramid height " + invalidInputText);
            inputHeight(length, width);
        }
    })
}

module.exports = { startCalculatePyramid }