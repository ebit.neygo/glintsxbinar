// Import readline
const { rl } = require('./utils')


const cuboid = require("./cuboid"); // import cuboid
const cube = require("./cube"); // import cube
const cone = require("./cone"); // import cone
const sphere = require("./sphere") // import sphere
const prism = require("./triangularPrism") // import prism
const pyramid = require("./pyramid") // import pyramid

// Function to display the menu
function menu() {
    console.log("=".repeat(process.stdout.columns));
    console.log(`Menu`);
    console.log("=".repeat(process.stdout.columns));
    console.log(`1. Cuboid`);
    console.log(`2. Cube`);
    console.log(`3. Cone`);
    console.log(`4. Sphere`);
    console.log(`5. Prism`);
    console.log(`6. Pyramid`)
    console.log(`7. Exit`);
    rl.question(`Choose option: `, (option) => {
        switch (option) {
            case '1':
                cuboid.inputLength();
                break;
            case '2':
                cube.input();
                break;
            case '3':
                cone.inputRadius();
                break;
            case '4':
                sphere.calSphere();
                break;
            case '5':
                prism.inputBase();
                break;
            case '6':
                pyramid.startCalculatePyramid();
                break;
            case '7':
                rl.close();
                break;
            default:
                console.log(`Option must be number!\n`);
                menu();
        }
    })
}

menu(); // call the menu function to display the menu