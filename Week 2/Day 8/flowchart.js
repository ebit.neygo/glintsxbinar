const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.on("close", () => {
    process.exit();
});

function goingToCinema() {
    console.log("Going to cinema!");
    setTimeout(() => checkcinemaopenornot(), 3000)
}

function checkcinemaopenornot() {
    rl.question("Check cinema open or not (Y/N)", (isOpen) => {
            if (isOpen === "Y" || isOpen === "y") {
                console.log("The cinema is open");
                setTimeout(() => buyTheticket(), 3000);
            } else if (isOpen === "N" || isOpen === "n") {
                console.log("The cinema is closed");
                setTimeout(() => goBackhome(), 3000);
            } else {
              console.log("Wrong Input, try again");
            }
        })
    }

    function buyTheticket() {
        console.log("buy the ticket!");
        console.log("the ticket is ready!");
        setTimeout(() => goTheticket(), 3000);
    }

    function goTheticket() {
        console.log("Waiting");
        setTimeout(() => goTotheroom(), 3000);
    }

    function goTotheroom() {
        console.log("Enter the movie room");
        setTimeout(() => watchingThemovie(), 3000);
    }

    function watchingThemovie() {
        console.log(`Enjoying the movie\nFinish watching the movie `);
        setTimeout(() => goBackhome(), 3000);
    }

    function goBackhome() {
        console.log("Go back home");
        console.log("End!");
        rl.close();
    }

    function Cinemacomplete() {
        console.log('Start');
        goingToCinema();
        // checkcinemaopenornot();
        // buyTheticket();
        // goTheticket();
        // watchingThemovie();
        // goBackhome();

        
    }

    Cinemacomplete();
