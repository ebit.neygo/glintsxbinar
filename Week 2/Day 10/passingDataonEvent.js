const EventEmitter = require('events'); // Import
const readline = require('readline');
const test = require("./assignment2.js");

 // Initialize an instance because it is a class
const my = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

// Registering a listener
my.on("Login Failed",(email,password) => {
  // TODO: Saving the login trial count in the database
  console.log(`${email} and ${password} failed to Login`);
  rl.close();
})
my.on('Login Success',(email) =>{
    console.log(`${email} is success to Login`);
    // test.showPos();
    require("./assignment2.js");
    rl.close();
});

function login(email, password){
    const passwordInDatabase = 12345;
    if(password != passwordInDatabase){
        my.emit('Login Failed',email,password);
    }
    else{
        my.emit('Login Success',email);
    }
}

  rl.question("Email: ", (email) => {
    rl.question("Password: ",(password) => {
      login(email, password) // Run login function
    })
  })
  