/* Menghitung Volume Kerucut */
function calKerucut(radius,tinggi){
    return  1/3 *Math.PI *radius**2*tinggi;
}

/* Menghitung Volume BOla */
function calBola(radius){
    return 4/3 * Math.PI * radius**3;
}
let volumeKerucut = calKerucut(7,7);
let volumeBola = calBola(7);
let totalVolume = volumeKerucut + volumeBola;

console.log('Volume Kerucut = '+volumeKerucut);
console.log('Volume Bola = '+volumeBola);
console.log('Total Volume Keduanya = '+totalVolume);
